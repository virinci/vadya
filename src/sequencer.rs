#![allow(dead_code)]
use rumi::{Event, EventIter, StandardMidiFile};

pub struct Sequencer<'a> {
    sources: Vec<MidiSource<'a>>,
}

impl Sequencer<'_> {
    fn new() -> Self {
        todo!()
    }
}

/// A one-track MIDI file like object.
struct LiveMidiSource<'a> {
    ticks_per_quarter_note: u16,
    time_per_tick: Vec<u32>,

    event_buffer: Vec<Option<Event<'a>>>,
    tracks: Vec<EventIter<'a>>,
}

enum MidiSource<'a> {
    StandardMidiFile(StandardMidiFile<'a>),
    Live(LiveMidiSource<'a>),
}

/*
impl MidiSource<'_> {
    fn iter(&self) -> SaneTrackIter {
        match self {
            &MidiSource::StandardMidiFile(smf) => {
                SaneTrackIter::new(&smf)
            }
            &MidiSource::Live(live) => {
                todo!()
            }
        }
    }
}
*/
