use cpal::traits::{DeviceTrait, HostTrait, StreamTrait};
use cpal::{Device, SampleRate, Stream, StreamConfig};
const BUFFER_LEN: u32 = 1 << 10;

fn audio_setup() -> (Device, StreamConfig) {
    let host = cpal::default_host();
    // dbg!(host.id());

    let device = host
        .default_output_device()
        .expect("no output device available");

    // dbg!(device.name().unwrap());
    // dbg!(device.default_output_config().unwrap());

    /*
    let config_range = device
        .supported_output_configs()
        .unwrap()
        .find(|c| c.channels() == 1 && c.sample_format() == SampleFormat::F32)
        .unwrap();
        */

    // dbg!(config_range.buffer_size());

    use cpal::BufferSize;

    let config = StreamConfig {
        channels: 1,
        sample_rate: SampleRate(44_100),
        buffer_size: BufferSize::Fixed(BUFFER_LEN),
    };

    (device, config)
}

pub struct Player {
    stream: Stream,
}

impl Player {
    pub fn new<F>(callback: F) -> Self
    where
        F: Fn(&mut [f32], &cpal::OutputCallbackInfo) + Send + 'static,
    {
        let (device, config) = audio_setup();
        let err_fn = |err| eprintln!("an error occurred on the output audio stream: {}", err);
        let stream = device
            .build_output_stream(&config, callback, err_fn, None)
            .unwrap();

        Self { stream }
    }

    pub fn play(&self) {
        self.stream.play().unwrap();
    }
}
