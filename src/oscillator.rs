fn angular_frequency(frequency: f64) -> f64 {
    use std::f64::consts::TAU;
    TAU * frequency
}

#[allow(dead_code)]
pub enum Osc {
    Sine,
    Square,
    Triangle,
    Sawtooth,
    SawtoothAnalog,
    Noise,
}

pub fn oscillate(
    frequency: f64,
    time: f64,
    oscillator_type: Osc,
    lfo_frequency: f64,
    lfo_amplitude: f64,
) -> f64 {
    use std::f64::consts::{FRAC_2_PI, TAU};

    let modulated_tau_time =
        TAU * time + lfo_amplitude * (angular_frequency(lfo_frequency) * time).sin();
    let modulated_tau_time_frequency = frequency * modulated_tau_time;

    match oscillator_type {
        Osc::Sine => modulated_tau_time_frequency.sin(),
        Osc::Square => modulated_tau_time_frequency.sin().signum(),
        Osc::Triangle => modulated_tau_time_frequency.sin().asin() * FRAC_2_PI,
        Osc::Sawtooth => {
            let time_period = 1.0 / frequency;

            // The modulated sawtooth wave just uses the `modulated_time` instead of `time`.
            let modulated_time = modulated_tau_time / TAU;
            assert!(modulated_time >= 0.0);

            // Use a method that uses less number of operations.
            // use std::f64::consts::{FRAC_PI_2, PI};
            // FRAC_2_PI * (frequency * PI * (modulated_time % time_period) - FRAC_PI_2)
            2.0 * (modulated_time % time_period) * frequency - 1.0
        }
        Osc::SawtoothAnalog => {
            const SAWTOOTH_MAX: u32 = 40;

            /*
            let mut sum = 0.0;
            for n in 1..=SAWTOOTH_MAX {
                sum += (angular_frequency(frequency) * time * n as f64).sin() / n as f64;
            }
            sum * FRAC_2_PI
            */

            (1..=SAWTOOTH_MAX)
                .map(|n| (modulated_tau_time_frequency * n as f64).sin() / n as f64)
                .sum::<f64>()
                * FRAC_2_PI
        }
        Osc::Noise => {
            if frequency != 0.0 {
                2.0 * rng::random() - 1.0
            } else {
                0.0
            }
        }
    }
}

// A1 * sin(2*pi*f1*t + A2 * sin(2*pi*t*f2))
// A1 * sin(2*pi*f1*t * (1 + A2/(2*pi*t) * sin(2*pi*t*f2)))

trait Oscillator {
    fn oscillate(time: f64) -> f64;
}
