use crate::note::{Note, NoteEvent, NoteEventStatus};
use crate::Adsr;

macro_rules! scale {
    ($index:expr) => {
        Note::new($index).frequency(0)
    };
}

pub trait Instrument {
    /// If the `note_event` is over with respect to the current `Instrument`.
    /// This takes into consideration the `release_time` of the corresponding envelopes of each
    /// `Instrument`.
    fn note_event_status(&self, note_event: &NoteEvent, time: f64) -> NoteEventStatus;

    // TODO: rename this method to `sample` (verb)
    fn sound(&self, time: f64, note_event: &mut NoteEvent) -> f64;
}

pub struct Harmonica {
    volume: f64,
    envelope: Adsr,
}

#[allow(dead_code)]
impl Harmonica {
    pub fn new(volume: f64) -> Self {
        let envelope = Adsr::new(0.01, 0.01, 0.02, 1.0, 0.8);
        Self { volume, envelope }
    }
}

impl Instrument for Harmonica {
    #[inline]
    fn note_event_status(&self, note_event: &NoteEvent, time: f64) -> NoteEventStatus {
        if time < note_event.on_time {
            NoteEventStatus::NotStarted
        } else if let Some(off_time) = note_event.off_time {
            if time < off_time + self.envelope.release_time {
                NoteEventStatus::Playing
            } else {
                NoteEventStatus::Ended
            }
        } else {
            NoteEventStatus::MaybePlaying
        }
    }

    fn sound(&self, time: f64, note_event: &mut NoteEvent) -> f64 {
        use crate::{oscillate, Osc};

        if !note_event.active {
            return 0.0;
        }

        let amplitude =
            self.envelope
                .amplitude(time, Some(note_event.on_time), note_event.off_time);

        const EPSILON: f64 = 1e-6;
        if amplitude == 0.0 {
            if time >= note_event.on_time + EPSILON {
                if note_event.off_time.is_none() {
                    dbg!(note_event.on_time, time);
                } else {
                    assert!(note_event.off_time.is_some());
                }

                let inactive_time = note_event.off_time.unwrap() + self.envelope.release_time;
                assert!(time >= inactive_time || time - inactive_time < 1e-4);
                note_event.active = false;
            }

            return 0.0;
        }

        let frequency = note_event.note().frequency(0);

        let wave = 1.0 * oscillate(frequency, time, Osc::Square, 5.0, 0.001)
            + 0.5 * oscillate(frequency * 1.5, time, Osc::Square, 0.0, 0.0)
            + 0.25 * oscillate(frequency * 2.0, time, Osc::Square, 0.0, 0.0)
            + 0.05 * oscillate(0.0, time, Osc::Noise, 0.0, 0.0);

        amplitude * wave * self.volume
    }
}

pub struct Bell {
    volume: f64,
    envelope: Adsr,
}

#[allow(dead_code)]
impl Bell {
    pub fn new(volume: f64) -> Self {
        let envelope = Adsr::new(0.01, 1.0, 1.0, 1.0, 0.8);
        Self { volume, envelope }
    }
}

impl Instrument for Bell {
    #[inline]
    fn note_event_status(&self, note_event: &NoteEvent, time: f64) -> NoteEventStatus {
        if time < note_event.on_time {
            NoteEventStatus::NotStarted
        } else if let Some(off_time) = note_event.off_time {
            if time < off_time + self.envelope.release_time {
                NoteEventStatus::Playing
            } else {
                NoteEventStatus::Ended
            }
        } else {
            NoteEventStatus::MaybePlaying
        }
    }

    fn sound(&self, time: f64, note_event: &mut NoteEvent) -> f64 {
        use crate::{oscillate, Osc};

        if !note_event.active || time < note_event.on_time {
            return 0.0;
        }

        let amplitude =
            self.envelope
                .amplitude(time, Some(note_event.on_time), note_event.off_time);

        const EPSILON: f64 = 1e-6;
        if amplitude == 0.0 {
            if time >= note_event.on_time + EPSILON {
                if note_event.off_time.is_none() {
                    dbg!(note_event.on_time, time);
                } else {
                    assert!(note_event.off_time.is_some());
                }

                let inactive_time = note_event.off_time.unwrap() + self.envelope.release_time;
                assert!(time >= inactive_time || time - inactive_time < 1e-4);
                note_event.active = false;
            }

            return 0.0;
        }

        let note_life_time = time - note_event.on_time;
        assert!(note_life_time >= 0.0);

        let note_index = note_event.note().index();

        // frequency, frequency * 2.0, frequency * 3.0
        let wave =
            1.0 * oscillate(
                scale!(note_index + 12),
                note_life_time,
                Osc::Sine,
                5.0,
                0.001,
            ) + 0.5 * oscillate(scale!(note_index + 24), note_life_time, Osc::Sine, 0.0, 0.0)
                + 0.25 * oscillate(scale!(note_index + 36), note_life_time, Osc::Sine, 0.0, 0.0);

        amplitude * wave * self.volume
    }
}

pub struct Piano {
    volume: f64,
    envelope: Adsr,
}

#[allow(dead_code)]
impl Piano {
    pub fn new(volume: f64) -> Self {
        let envelope = Adsr::new(0.01, 0.01, 0.5, 1.0, 0.6);
        Self { volume, envelope }
    }
}

impl Instrument for Piano {
    #[inline]
    fn note_event_status(&self, note_event: &NoteEvent, time: f64) -> NoteEventStatus {
        if time < note_event.on_time {
            NoteEventStatus::NotStarted
        } else if let Some(off_time) = note_event.off_time {
            if time < off_time + self.envelope.release_time {
                NoteEventStatus::Playing
            } else {
                NoteEventStatus::Ended
            }
        } else {
            NoteEventStatus::MaybePlaying
        }
    }

    fn sound(&self, time: f64, note_event: &mut NoteEvent) -> f64 {
        if !note_event.active {
            return 0.0;
        }

        let amplitude =
            self.envelope
                .amplitude(time, Some(note_event.on_time), note_event.off_time);

        assert!(note_event.active);

        let status = self.note_event_status(note_event, time);
        match status {
            NoteEventStatus::NotStarted => assert_eq!(amplitude, 0.0),
            NoteEventStatus::Playing | NoteEventStatus::MaybePlaying => {
                // assert_ne!(amplitude, 0.0);
            }
            NoteEventStatus::Ended => unreachable!(),
        }

        let frequency = note_event.note().frequency(0);
        // Y = sin(2 * pi * frequency * time) * exp(-0.0004 * 2 * pi * frequency * time);

        use std::f64::consts::TAU;

        let tau_frequency_time = TAU * frequency * (time - note_event.on_time);

        let factor = match note_event.off_time {
            Some(off_time) if time >= off_time => {
                // let tau_frequency_time = TAU * frequency * (time - off_time);
                (-0.0004 * tau_frequency_time).exp()
                // 0.5
            }
            _ => {
                // 0.2
                (-0.00004 * tau_frequency_time).exp()
            }
        };

        // let factor = (-0.0004 * tau_frequency_time).exp();

        use crate::{oscillate, Osc};

        let note_life_time = time - note_event.on_time;
        // let mut wave = oscillate(frequency, note_life_time, Osc::Sine, 1.0, 0.01) * factor;

        let mut wave = tau_frequency_time.sin() * factor;
        wave += (2.0 * tau_frequency_time).sin() * factor;

        /*
        wave += (0.5 * tau_frequency_time).sin() * factor * 0.25;
        wave += (2.0 * tau_frequency_time).sin() * factor * 0.125;
        */

        /*
        wave += (3.0 * tau_frequency_time).sin() * factor;
        wave += (4.0 * tau_frequency_time).sin() * factor;
        wave += (5.0 * tau_frequency_time).sin() * factor;
        wave += (6.0 * tau_frequency_time).sin() * factor;
        */

        wave += wave * wave * wave;

        amplitude * wave * self.volume
    }
}
