use crate::instrument::{self, Instrument};
use crate::note::NoteEvent;
use std::time::Instant;

type InstrumentPtr = Box<dyn Instrument + Send + Sync>;

pub struct Synthesizer {
    done_time: f64,
    sample_rate: u32,

    // TODO: consider using a `VecDeque` instead as popping from the front is cheaper in it
    pub note_events: Vec<NoteEvent>,

    start_time: Instant,

    instrument: InstrumentPtr,
}

impl Synthesizer {
    pub fn new() -> Self {
        Self {
            done_time: 0.0,
            sample_rate: 44_100,

            note_events: Vec::with_capacity(128),

            start_time: Instant::now(),
            instrument: Box::new(instrument::Piano::new(0.2)),
        }
    }

    pub fn time(&self) -> f64 {
        self.start_time.elapsed().as_secs_f64()
    }

    pub fn start(&mut self) {
        self.start_time = Instant::now();
    }

    pub fn write_samples(&mut self, data: &mut [f32], _: &cpal::OutputCallbackInfo) {
        const VOLUME: f64 = 0.2;
        let dt = 1.0 / self.sample_rate as f64;

        assert!(self.note_events.iter().all(|note_event| note_event.active));

        for sample in data.iter_mut() {
            self.note_events.iter_mut().for_each(|ev| {
                use crate::note::NoteEventStatus;
                if let NoteEventStatus::Ended =
                    self.instrument.note_event_status(ev, self.done_time)
                {
                    ev.active = false;
                }
            });

            let raw_sample = self
                .note_events
                .iter_mut()
                .filter_map(|note_event| {
                    (note_event.active && self.done_time >= note_event.on_time)
                        .then(|| self.instrument.sound(self.done_time, note_event))
                })
                .sum::<f64>();

            /*
            let mut raw_sample = 0.0;
            for note_event in self.note_events.iter_mut() {
                raw_sample += self.instrument.sound(self.done_time, note_event);
            }
            */

            const MAX_AMPLITUDE: f64 = 50.0;
            let raw_sample = raw_sample.clamp(-MAX_AMPLITUDE, MAX_AMPLITUDE);

            *sample = (VOLUME * raw_sample) as f32;
            self.done_time += dt;
        }

        self.note_events.retain(|ev| ev.active);
    }
}
