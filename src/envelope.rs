#[derive(Default)]
pub struct Adsr {
    attack_time: f64,
    decay_time: f64,
    pub(crate) release_time: f64,

    /// The amplitude with which the waveform starts when the key is pressed.
    attack_amplitude: f64,

    /// The amplitude which is sustained when the key is being pressed.
    sustain_amplitude: f64,
}

impl Adsr {
    pub fn new(
        attack_time: f64,
        decay_time: f64,
        release_time: f64,
        attack_amplitude: f64,
        sustain_amplitude: f64,
    ) -> Self {
        Self {
            attack_time,
            decay_time,
            release_time,
            attack_amplitude,
            sustain_amplitude,
        }
    }

    /// `time` is exclusive on the upper bound of each phase of the ADSR envelope.
    /// So, if the `time < decay_time` then it's still in attack phase, but if `time == decay_time`
    /// then it goes into the decay phase.
    #[allow(dead_code)]
    pub fn amplitude2(&self, time: f64, on_time: Option<f64>, off_time: Option<f64>) -> f64 {
        // if (time % 1.0).abs() < 1e-4 { println!("amplitude queried at {}", time); }
        if on_time.is_none() && off_time.is_none() {
            return 0.0;
        }

        let note_on = on_time.is_some() && off_time.is_none();

        let current_amplitude = if note_on {
            let trigger_on_time = on_time.unwrap();
            let life_time = time - trigger_on_time;

            if life_time < 0.0 {
                0.0
            } else if life_time < self.attack_time {
                range_map(0.0, self.attack_time, 0.0, self.attack_amplitude, life_time)
            } else if life_time < self.attack_time + self.decay_time {
                range_map(
                    0.0,
                    self.decay_time,
                    self.attack_amplitude,
                    self.sustain_amplitude,
                    life_time - self.attack_time,
                )
            } else {
                self.sustain_amplitude
            }
        } else {
            let trigger_off_time = off_time.unwrap();
            let after_life = time - trigger_off_time;

            if after_life < 0.0 {
                self.sustain_amplitude
            } else if after_life < self.release_time {
                // after_life / self.release_time * -self.sustain_amplitude + self.sustain_amplitude
                range_map(
                    0.0,
                    self.release_time,
                    self.sustain_amplitude,
                    0.0,
                    after_life,
                )
            } else {
                0.0
            }
        };

        if current_amplitude <= 1e-4 {
            0.0
        } else {
            current_amplitude
        }
    }
}

#[allow(dead_code)]
#[inline]
fn lerp<T>(x: T, y: T, a: T) -> T
where
    T: Copy + std::ops::Add<Output = T> + std::ops::Sub<Output = T> + std::ops::Mul<Output = T>,
{
    x + (y - x) * a
}

#[inline]
fn range_map<T>(a1: T, a2: T, b1: T, b2: T, s: T) -> T
where
    T: Copy
        + std::ops::Add<Output = T>
        + std::ops::Sub<Output = T>
        + std::ops::Mul<Output = T>
        + std::ops::Div<Output = T>,
{
    b1 + (s - a1) * (b2 - b1) / (a2 - a1)
}

impl Adsr {
    fn amplitude_after_on(&self, time: f64, on_time: f64) -> f64 {
        assert!(time >= on_time);

        let life_time = time - on_time;

        // TODO: get rid of this first branch
        if life_time < 0.0 {
            0.0
        } else if life_time < self.attack_time {
            range_map(0.0, self.attack_time, 0.0, self.attack_amplitude, life_time)
        } else if life_time < self.attack_time + self.decay_time {
            range_map(
                0.0,
                self.decay_time,
                self.attack_amplitude,
                self.sustain_amplitude,
                life_time - self.attack_time,
            )
        } else {
            self.sustain_amplitude
        }
    }

    fn amplitude_after_off(&self, time: f64, off_time: f64) -> f64 {
        assert!(time >= off_time);

        let after_life = time - off_time;

        // TODO: get rid of this first branch
        if after_life < 0.0 {
            self.sustain_amplitude
        } else if after_life < self.release_time {
            // after_life / self.release_time * -self.sustain_amplitude + self.sustain_amplitude
            range_map(
                0.0,
                self.release_time,
                self.sustain_amplitude,
                0.0,
                after_life,
            )
        } else {
            0.0
        }
    }

    // TODO: fix the releasing the key while still in attack phase bug
    pub fn amplitude(&self, time: f64, on_time: Option<f64>, off_time: Option<f64>) -> f64 {
        // if (time % 1.0).abs() < 1e-4 { println!("amplitude queried at {}", time); }

        if on_time.is_none() {
            assert!(off_time.is_none())
        }

        let current_amplitude = match (on_time, off_time) {
            (Some(on_time), Some(off_time)) => {
                // assert!(off_time > on_time);

                if time < on_time {
                    0.0
                } else if time < off_time {
                    self.amplitude_after_on(time, on_time)
                } else {
                    self.amplitude_after_off(time, off_time)
                }
            }
            (Some(on_time), None) => {
                if time < on_time {
                    0.0
                } else {
                    self.amplitude_after_on(time, on_time)
                }
            }
            (None, None) => 0.0,
            _ => unreachable!(),
        };

        if current_amplitude <= 1e-4 {
            0.0
        } else {
            current_amplitude
        }
    }
}
