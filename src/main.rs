mod envelope;
mod instrument;
mod note;
mod oscillator;
mod player;
mod sane_iter;
mod sequencer;
mod synthesizer;

use envelope::Adsr;
use note::NoteEvent;
use oscillator::{oscillate, Osc};
use rumi::StandardMidiFile;
use sane_iter::SaneTrackIter;

use crate::note::Note;

fn main() {
    let midi_path = std::env::args()
        .nth(1)
        .expect("ERROR: MIDI file path not provided as argument");
    let stream = &std::fs::read(midi_path).unwrap();

    let banner = include_str!("../banner.txt");
    print!("{}", banner);

    use sfml::window::{Event, Key, Style, Window};
    let mut window = Window::new(
        (50, 50),
        "Rynth - DEBUG",
        Style::DEFAULT,
        &Default::default(),
    );

    // window.set_framerate_limit(60);
    window.set_key_repeat_enabled(false);

    use std::sync::{Arc, Mutex};
    use synthesizer::Synthesizer;
    let synthesizer = Arc::new(Mutex::new(Synthesizer::new()));

    let write_samples = {
        let synthesizer = synthesizer.clone();
        move |data: &mut [_], callback_info: &cpal::OutputCallbackInfo| {
            synthesizer
                .lock()
                .unwrap()
                .write_samples(data, callback_info);
        }
    };

    use player::Player;
    let player = Player::new(write_samples);

    synthesizer.lock().unwrap().start();
    player.play();

    let keys = [
        Key::Z,
        Key::S,
        Key::X,
        Key::C, // C
        Key::F, // C#
        Key::V, // D
        Key::G, // D#
        Key::B, // E
        Key::N, // F
        Key::J, // F#
        Key::M, // G
        Key::K,
        Key::Comma,
        Key::L,
        Key::Period,
        Key::Slash,
    ];

    let smf = StandardMidiFile::new(stream);
    let mut sane_iter = SaneTrackIter::new(&smf);
    let mut notes = [None; 128];
    let mut time = 0.0;

    let mut event_info = None;

    while window.is_open() {
        while event_info.clone().is_none() {
            let Some(next_event_info) = sane_iter.next() else {
                break;
            };
            /*
            if next_event_info.1 != 1 {
                continue;
            }
            */

            event_info = Some(next_event_info);
        }

        if event_info.is_none() {
            dbg!();
            continue;
        }

        assert!(event_info.is_some());
        let event_info_inner = event_info.clone().unwrap();

        let synthesizer_time = {
            let s = synthesizer.lock().unwrap();
            s.time()
        };

        let delta_time = event_info_inner.0;
        let dt = time + delta_time * 1e-6 - synthesizer_time;

        if dt < 0.1 {
            let (delta_time, _track_index, ev) = event_info_inner;
            event_info = None;

            time += delta_time * 1e-6;
            let event = ev.event();
            use rumi::{EventKind, MidiEvent};

            match event {
                EventKind::Midi(MidiEvent::NoteOn {
                    channel: _,
                    note,
                    velocity: _,
                })
                | EventKind::Midi(MidiEvent::NoteOff {
                    channel: _,
                    note,
                    velocity: _,
                }) => {
                    let mut synthesizer = synthesizer.lock().unwrap();
                    let index = note - 21;

                    if notes[note as usize].is_none() {
                        let event = NoteEvent::new(Note::new(index), time, 0);

                        synthesizer.note_events.push(event);
                        notes[note as usize] = Some(time);
                    } else {
                        synthesizer
                            .note_events
                            .iter_mut()
                            .rev()
                            .filter(|note_event| note_event.note.index() == index)
                            .take(1)
                            .for_each(|note_event| note_event.set_off_time(time));
                        notes[note as usize] = None;
                    }
                }
                _ => {}
            }
        }

        while let Some(event) = window.poll_event() {
            match event {
                Event::Closed => {
                    window.close();
                }
                Event::KeyPressed {
                    code,
                    alt: false,
                    ctrl: false,
                    shift: false,
                    system: false,
                    ..
                } => {
                    if let Some(idx) = keys.iter().position(|&k| k == code) {
                        let mut synthesizer = synthesizer.lock().unwrap();

                        // synthesizer.frequency = octave_base_frequency * scaler.powi(idx as _);

                        let time = synthesizer.time();

                        // TODO: fix multiple events related to the same note spike up volume when
                        // the key is spammed

                        let count = 0;

                        /*
                        // Case for when the key has been pressed down again during release phase.
                        synthesizer
                            .note_events
                            .iter_mut()
                            .filter(|note_event| {
                                note_event.note.index() == idx as _ && note_event.off_time.is_some()
                            })
                            .for_each(|note_event| {
                                count += 1;
                                // note_event.set_off_time(time);
                                note_event.on_time = time;
                                note_event.active = true;
                            });
                        assert!(count <= 1);
                            */

                        if count == 0 {
                            synthesizer.note_events.push(NoteEvent::new(
                                note::Note::new((idx + 60 - 21) as _),
                                time,
                                0,
                            ));
                        }

                        // println!("{:?} pressed at {}", keys[idx], time);
                        // println!("note_event: {:?}", synthesizer.note_event);
                    }
                }
                Event::KeyReleased {
                    code,
                    alt: false,
                    ctrl: false,
                    shift: false,
                    system: false,
                } => {
                    if let Some(idx) = keys.iter().position(|&k| k == code) {
                        let mut synthesizer = synthesizer.lock().unwrap();

                        let time = synthesizer.time();
                        synthesizer
                            .note_events
                            .iter_mut()
                            .filter(|note_event| note_event.note.index() == (idx + 60 - 21) as _)
                            .for_each(|note_event| {
                                note_event.set_off_time(time);
                            });

                        // println!("{:?} released at {}", keys[_idx], time);
                        // println!("note_event: {:?}", synthesizer.note_event);
                    }
                }
                _ => {}
            }
        }

        window.display();
    }
}
