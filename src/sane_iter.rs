use rumi::{Division, Event, EventIter, Format, StandardMidiFile, TrackChunk};

pub struct SaneTrackIter<'a> {
    format: Format,
    ticks_per_quarter_note: u16,
    time_per_tick: Vec<f64>,

    event_buffer: Vec<Option<Event<'a>>>,
    tracks: Vec<EventIter<'a>>,
}

impl<'a> Iterator for SaneTrackIter<'a> {
    type Item = (f64, u16, Event<'a>);
    fn next(&mut self) -> Option<Self::Item> {
        for (event, track) in self.event_buffer.iter_mut().zip(self.tracks.iter_mut()) {
            if event.is_none() {
                *event = track.next();
            }
        }

        if self.event_buffer.iter().all(|event| event.is_none()) {
            return None;
        }

        let min_idx = (0..self.time_per_tick.len())
            .filter(|&i| self.event_buffer[i].is_some())
            .min_by(|&a, &b| {
                use std::cmp::Ordering;

                if self.event_buffer[a].is_none() || self.event_buffer[b].is_none() {
                    return Ordering::Equal;
                }

                (self.time_per_tick[a] * self.event_buffer[a].clone().unwrap().delta_ticks() as f64)
                    .partial_cmp(
                        &(self.time_per_tick[b]
                            * self.event_buffer[b].clone().unwrap().delta_ticks() as f64),
                    )
                    .unwrap()
            });

        assert!(min_idx.is_some());
        let min_idx = min_idx.unwrap();

        let event = self.event_buffer[min_idx].clone().unwrap();
        self.event_buffer[min_idx] = self.tracks[min_idx].next();

        let time = self.time_per_tick[min_idx] * event.delta_ticks() as f64;

        use rumi::{EventKind, MetaEvent};
        if let EventKind::Meta(MetaEvent::SetTempo(tempo)) = event.event() {
            if let Format::SingleMultiChannel = self.format {
                self.time_per_tick[min_idx] = tempo as f64 / self.ticks_per_quarter_note as f64;
            } else if let Format::Simultaneous = self.format {
                // unimplemented!();
                for t in self.time_per_tick.iter_mut() {
                    *t = tempo as f64 / self.ticks_per_quarter_note as f64;
                }
            } else {
                unimplemented!();
            }
        }

        Some((time, min_idx as _, event))
    }
}

impl<'a> SaneTrackIter<'a> {
    pub fn new(smf: &'a StandardMidiFile) -> Self {
        let header = smf.header();
        let Division::TicksPerQuarterNote(ticks_per_quarter_note) = header.division() else {
            unreachable!()
        };

        let tracks = smf.tracks().collect::<Vec<TrackChunk>>();
        let num_tracks = header.number_of_tracks() as usize;

        Self {
            format: header.format(),
            ticks_per_quarter_note,
            time_per_tick: vec![0.0; num_tracks],
            event_buffer: vec![None; num_tracks],
            tracks: tracks
                .into_iter()
                .map(|track| track.into_iter())
                .collect::<Vec<EventIter>>(),
        }
    }
}
