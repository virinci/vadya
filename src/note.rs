use std::collections::VecDeque;

/// The status of a `NoteEvent` at any given time.
pub enum NoteEventStatus {
    NotStarted,
    Playing,
    MaybePlaying,
    Ended,
}

/// Position of this note in the scale.
#[derive(Clone, Copy, Debug)]
pub struct Note(u8);

impl Note {
    pub fn new(index: u8) -> Self {
        Self(index)
    }

    pub fn index(&self) -> u8 {
        self.0
    }

    /// Find the frequency related to this note using this note's index.
    pub fn frequency(&self, _scale: u8) -> f64 {
        const SEMI_TONE: f64 = 1.0594630943592953;

        // let scale_frequency = 110.0 * scale as f64;
        // scale_frequency * SEMI_TONE.powi(self.0 as _)

        // 256.0 * SEMI_TONE.powi(self.0 as _)
        // 220.0 * SEMI_TONE.powi(self.0 as _)
        27.5 * SEMI_TONE.powi(self.0 as _)
    }
}

#[derive(Debug)]
pub struct NoteEvent {
    pub(crate) note: Note,
    pub(crate) on_time: f64,
    pub(crate) off_time: Option<f64>,
    pub(crate) active: bool,

    #[allow(dead_code)]
    channel: u8,
}

impl NoteEvent {
    pub fn new(note: Note, on_time: f64, channel: u8) -> Self {
        Self {
            note,
            on_time,
            off_time: None,
            active: true,
            channel,
        }
    }

    pub fn set_off_time(&mut self, off_time: f64) {
        self.off_time = Some(off_time);
    }

    pub fn note(&self) -> Note {
        self.note
    }
}

#[allow(dead_code)]
#[derive(Default)]
struct NoteEventQueue {
    events: VecDeque<NoteEvent>,
    scratch: Vec<NoteEvent>,
}

#[allow(dead_code)]
impl NoteEventQueue {
    fn new() -> Self {
        Default::default()
    }

    fn events_till(&mut self, time: f64) -> &[NoteEvent] {
        self.events.make_contiguous();
        let (events, _) = self.events.as_slices();

        if let Some(idx) = events.iter().position(|ev| ev.on_time >= time) {
            &events[..idx]
        } else {
            events
        }
    }

    fn trim_inactive(&mut self) {
        self.trim_inactive_till(f64::INFINITY);
    }

    fn trim_inactive_till(&mut self, time: f64) {
        while let Some(ev) = self.events.front() {
            if ev.on_time < time {
                break;
            }

            if ev.active {
                self.scratch.push(self.events.pop_front().unwrap());
            }
        }

        while let Some(ev) = self.scratch.pop() {
            self.events.push_front(ev);
        }
    }
}
