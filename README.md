# vadya

A music synthesizer and sequencer.

## Demo

![Play demo.mp4](./demo.mp4)

## Instructions

```
cargo r --release -- /path/to/midi/file
```
